#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'gitlab'
require_relative '../lib/changelog'

REL_FILE_PATH = "source/handbook/CHANGELOG.html.md"
FILE_PATH = File.expand_path(REL_FILE_PATH)

Changelog::File.create(FILE_PATH)

ZJ_ID = 101578
PROJECT_ID = WWW_GITLAB_COM_PROJECT_ID
TARGET_BRANCH = 'master'
GENERATED_TIMESTAMP = DateTime.now.to_date.to_s
SOURCE_BRANCH = "changelog-#{GENERATED_TIMESTAMP}"

Gitlab.create_branch(PROJECT_ID, SOURCE_BRANCH, TARGET_BRANCH)
Gitlab.create_commit(PROJECT_ID, SOURCE_BRANCH, "Update changelog for #{GENERATED_TIMESTAMP}",
                     [action: 'update', file_path: REL_FILE_PATH, content: File.read(FILE_PATH)])

mr = Gitlab.create_merge_request(PROJECT_ID, "Update changelog for #{GENERATED_TIMESTAMP}",
                            source_branch: SOURCE_BRANCH, target_branch: TARGET_BRANCH,
                            assignee_id: ZJ_ID, remove_source_branch: true,
                            labels: 'no changelog')

MergeCheck.wait_for_mergeability(mr)
Gitlab.accept_merge_request(PROJECT_ID, mr.iid, merge_when_pipeline_succeeds: true, should_remove_source_branch: true)
