# frozen_string_literal: true

require 'middleman'

#
# This middleman extension is used to speed up builds on CI
#
# We effectively do this by leveraging our CI parallelism features and split the build into six
# distinct partitions:
#
# 1. `PROXY_RESOURCE`: We leverage the `proxy` feature of middleman to generate e.g. job pages,
#    direction pages and others
# 2. `IMAGES`: We have a lot of images. So we let middleman copy them. Maybe we can de-middleman
#    it in the future
# 3. `BLOG_POST_OLD`: All blog posts up to 2017
# 4. `BLOG_POST_NEW`: All blog posts since 2018
# 5. `RELEASE_BLOG`: Release blog posts
# 6. `HANDBOOK_ENGINEERING`: The engineering handbook
# 7. `HANDBOOK_OTHER`: Other handbooks
# 8. `ASSETS`: javascripts, stylesheets, icons, and pdfs
# 9. `COMPANY`: All company pages and team images
# 10. `ALL_OTHERS`: All pages and files which do not fit into the categories above
#
# If `CI_NODE_INDEX` and `CI_NODE_TOTAL` are not set, e.g. on development machines, or turning
# parallelism off, it will simply be a noop extension
class PartialBuild < Middleman::Extension
  PROXY_RESOURCE = "proxy resources"
  IMAGES = "IMAGES"
  BLOG_POST_OLD = "blog posts old (up to 2017)"
  BLOG_POST_NEW = "blog posts new (since 2018)"
  RELEASE_BLOG = "release blog"
  HANDBOOK_ENGINEERING_MARKETING = "Engineering and marketing handbook"
  HANDBOOK_OTHER = "All other handbooks"
  ASSETS = "javascript, stylesheets, icons, pdfs"
  COMPANY = "Company pages and team images"
  ALL_OTHERS = "all other pages"

  # We must ensure that this extension runs last, so that
  # the filtering works correctly
  self.resource_list_manipulator_priority = 1000

  def_delegator :@app, :logger

  def initialize(app, options_hash = {}, &block)
    super
    @enabled = ENV['CI_NODE_INDEX'] && ENV['CI_NODE_TOTAL'] || ENV['CI_BUILD_PROXY_RESOURCE']

    if ENV['CI_BUILD_PROXY_RESOURCE']
      @partial = PROXY_RESOURCE
      return
    end

    return unless @enabled

    raise "#{self.class.name}: If you want to enable parallel builds, please use exactly 9 parallel jobs" unless ENV['CI_NODE_TOTAL'].to_i == 9

    @partial = case ENV['CI_NODE_INDEX']
               when "1"
                 IMAGES
               when "2"
                 BLOG_POST_OLD
               when "3"
                 BLOG_POST_NEW
               when "4"
                 RELEASE_BLOG
               when "5"
                 HANDBOOK_ENGINEERING_MARKETING
               when "6"
                 HANDBOOK_OTHER
               when "7"
                 ASSETS
               when "8"
                 COMPANY
               when "9"
                 ALL_OTHERS
               else
                 raise "#{self.class.name}: Invalid Build Partial #{ENV['CI_NODE_INDEX']}. At the moment we only support 1 to 9"
               end
  end

  def images?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?('images/', 'devops-tools/azure_devops/images/') && !company?(resource)
  end

  def blog_page_new?(resource)
    !proxy_resource?(resource) &&
      ((resource.destination_path.start_with?('blog/') && !blog_page_old?(resource)) ||
        resource.destination_path.end_with?('atom.xml'))
  end

  def blog_page_old?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'blog/2011',
        'blog/2012',
        'blog/2012',
        'blog/2013',
        'blog/2014',
        'blog/2015',
        'blog/2016',
        'blog/2017'
      )
  end

  def release_blog?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'releases/'
      )
  end

  def handbook_engineering_marketing?(resource)
    resource.destination_path.start_with?(
      'handbook/index.html',
      'handbook/engineering',
      'handbook/marketing'
    )
  end

  def handbook_other?(resource)
    !proxy_resource?(resource) &&
      (resource.destination_path.start_with?('handbook/') && !handbook_engineering_marketing?(resource))
  end

  def assets?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'ico/',
        'stylesheets/',
        'javascripts/',
        '/devops-tools/pdfs/',
        '/pdfs'
      )
  end

  def company?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?(
        'images/team',
        'company/team',
        'company/team-pets',
        'company/culture/all-remote/stories',
        'community/core-team'
      )
  end

  def proxy_resource?(resource)
    # handbook_engineering_marketing now handles its own proxy resources. They don't do remote API
    # calls, so they are not slow. This also is a move towards splitting up responsibility
    # along monorepo sub-site lines. It also means that the `ignore` option on `proxy`
    # works properly in the sub-sites, thus explicit ignores aren't needed in the sub-sites' configs.
    return false if handbook_engineering_marketing?(resource)

    # TODO: The rese of these need to eventually be moved to be the responsibility of individual sites
    resource.instance_of?(Middleman::Sitemap::ProxyResource) ||
      resource.destination_path.start_with?('templates/', 'direction/', 'sales/') ||
      resource.destination_path.end_with?('/template.html', 'category.html')
  end

  def all_others?(resource)
    !proxy_resource?(resource) &&
      !blog_page_new?(resource) &&
      !blog_page_old?(resource) &&
      !release_blog?(resource) &&
      !images?(resource) &&
      !handbook_engineering_marketing?(resource) &&
      !handbook_other?(resource) &&
      !assets?(resource) &&
      !company?(resource)
  end

  def part_of_partial?(resource)
    return true if resource.destination_path == 'sitemap.xml'

    case @partial
    when PROXY_RESOURCE
      proxy_resource?(resource)
    when IMAGES
      images?(resource)
    when BLOG_POST_OLD
      blog_page_old?(resource)
    when BLOG_POST_NEW
      blog_page_new?(resource)
    when RELEASE_BLOG
      release_blog?(resource)
    when HANDBOOK_OTHER
      handbook_other?(resource)
    when ASSETS
      assets?(resource)
    when COMPANY
      company?(resource)
    when ALL_OTHERS
      all_others?(resource)
    else
      raise "#{self.class.name}: You are trying to build a unknown partial: #{@partial}"
    end
  end

  def manipulate_resource_list(resources)
    unless @enabled
      logger.info "#{self.class.name}: CI environment variables were not set for a partial build; building everything"
      return resources
    end

    if @partial == HANDBOOK_ENGINEERING_MARKETING
      puts "Skipping build of `handbook/engineering` and `handbook/marketing` from #{self.class.name}; " \
        'it now is delegated to the `build-handbook` job.'
      # Ensure public directory exists to avoid errors
      FileUtils.mkdir_p(File.expand_path('../public', __dir__))
      return []
    end

    logger.info "#{self.class.name}: We are building the partial: #{@partial}"

    resources.select { |resource| part_of_partial?(resource) }
  end
end

::Middleman::Extensions.register(:partial_build, PartialBuild)
