---
layout: handbook-page-toc
title: "GitLab Offboarding FAQ"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Timing

**When will my offboarding issue be created?**

It will be created on your last day, the People Experience Associate assigned to your offboarding is responsible for confirming your log-off time with you.

**When will my access be deactivated?**

Your access will be deactivated on your last day, after the team member has logged off for the day. The People Experience Associate will communicate with the departing team member a day or two before their last day to confirm their final logoff time.

**When will I receive my final pay?**

This depends on your location. When you complete your Exit Interview, the People Operations Specialist will clarify that your final check (or invoice period) will be for the pay period of X and includes X days of pay.

**When do I submit my final reimbursable expenses?**

Please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner. We ask that you submit these at least one week before your departure date.

**How do I submit reimbursable expenses if I don’t have a GitLab access to Expensify?**

Finance provides an invoice template that can be emailed to your personal email. Return the completed form to `uspayroll@domain.com`

### Benefits

**When will my benefits coverage end?**

Typically, benefits coverage ends at the end of the month during which you leave.

If you are a US-based team member, your benefits will cease on last day of the month of your departure. You are eligible for Consolidated Omnibus Budget Reconciliation Act (“COBRA”), the carrier (Lumity) has been notified and the carrier will send out the paperwork to your home address on file. You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace. If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month. Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from the Compensation & Benefits team by emailing `total-rewards@domain.com`. If you move, please email `people-exp@domain.com` so that we can keep your address up to date in BambooHR.

**How do I roll over my retirement account and funds?**

If you are a US-based team member, contact Betterment directly to get more information about this process.

**How will my stock options be managed?**

If you joined after 2015, you have 90 days to purchase your stock via Carta.

### Other

**Can I purchase my laptop from GitLab?**

IT Ops will contact you after your last day to determine if this is a possibility.
